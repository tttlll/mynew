import vk
session = (vk.Session(access_token='token'))
api = vk.API(session)

def ban_unban(value,id):
    try:
        message='⚠ Проверьте правильность введенной команды!!!'

        if value=='add':
            z = api.users.get(user_ids=id, v=5.85)[0]
            fname = z.get('first_name')
            lname = z.get('last_name')

            api.account.ban(owner_id=id,v=5.85)
            message='⚠ Пользователь {} {} был добавлен в черный список'.format(fname,lname)
        elif value=='remove':

            z = api.users.get(user_ids=id, v=5.85)[0]
            fname = z.get('first_name')
            lname = z.get('last_name')

            api.account.unban(owner_id=id,v=5.85)
            message='⚠ Пользователь {} {} был удален из черного списка'.format(fname,lname)

        elif value=='info':

            info = api.account.getBanned(count=200, v=5.85)['items']
            message = ''
            for i in range(len(info)):
                fname = info[i].get('first_name')
                lname = info[i].get('last_name')
                id = info[i].get('id')
                message = message + '{}. {} {} - (@id{})\n'.format(i+1,fname, lname, id)

    except:
        message = '⚠ Проверьте правильность введенной команды'
    return message




