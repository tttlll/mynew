import vk
session = (vk.Session(access_token='token'))
api = vk.API(session)
#136287505
def user_info(user_id):
    try:
        z=api.users.get(v=5.85,user_ids=user_id,fields=('online','status','city','sex','followers_count','blacklisted_by_me'))[0]
        fri=api.friends.get(user_id=user_id,v=5.85)

        id=z.get('id')
        name=z.get('first_name')+' '+z.get('last_name')

        if z.get('sex')==1:
            sex='женский'
        elif z.get('sex')==2:
            sex='мужской'
        else:
            sex='неизвествен'

        status=z.get('status')
        if status=='':
            status='статус пуст'

        if z.get('online'):
            online='✅ Online'
        else:
            online='❌ Offline'

        friend=fri.get('count')

        follower=z.get('followers_count')
        if follower==None:
            follower=0

        if z.get('blacklisted_by_me'):
            black='✅'
        else:
            black='❌'

        city=z.get('city')
        if city!=None:
            city=city.get('title')
        else:
            city='город не указан'

        message='''
🔹 id - {}
🔹 Имя и фамилия - {}
🔹 Пол - {}
🔹 Статус - {}
🔹 Онлайн - {}
🔹 Город - {}
🔹 Количество друзей - {}
🔹 Количество подписчиков - {}
🔹 Добавил бота в чс - {}
        '''.format(id,name,sex,status,online,city,friend,follower,black)
    except:
        message='❌ Ошибка'
    return message
