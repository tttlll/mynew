import random


def When():
    try:
        num = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28,
               29, 30]
        month = ['января', 'Февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября',
                 'ноября', 'декабря']
        year = [2019, 2020, 2021, 2022, 2023, 2024]
        word = ['завтра', 'послезавтра', 'через неделю', 'никогда', 'позозавтро))0)']
        z = random.randint(1, 2)

        if z == 1:
            message = 'Я думаю, это произойдет {} {} {} года.'.format(num[random.randint(0, len(num) - 1)],
                                                                      month[random.randint(0, len(month) - 1)],
                                                                      year[random.randint(0, len(year) - 1)])
        else:
            message = 'Я думаю это произойдет {}.'.format(word[random.randint(0, len(word) - 1)])
    except:
        message = 'Critical error'
    return message