import bs4
import requests

def weather(city):
    try:
        message=''
        city=city.replace(" ","-")
        print(city)
        url='https://sinoptik.com.ru/погода-'+city

        r=requests.get(url)

        b=bs4.BeautifulSoup(r.text, "html.parser")
        #b=b.prettify()
        day=b.select('.day-link')
        temp=b.select('.temperature')
        month=b.select('.month')
        date=b.select('.date')


        for i in range(len(temp)-1):
           message=message+("🔹 {} {} ({})\nТемпература - {}\n\n".format(date[i].getText(),month[i].getText(),day[i].getText(),temp[i].getText()))
    except:
        message='Неверно найден город.\nВводите русскими буквами!'


    return message
