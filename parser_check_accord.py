import requests,bs4
from parser_accord import accord


def check_accord(name,id):
    try:
        id=int(id)
        name_url = 'https://mychords.net'
        url = 'https://mychords.net/search?q='+name

        r = requests.get(url)
        b = bs4.BeautifulSoup(r.text, "html.parser")
        list_music = b.select('.b-listing__item')
        href=list_music[id-1].find('a').get('href')
        href=accord(href)
    except:
        href = ''


    return href
