import requests,bs4

def name_accord(name):
    try:
        name_url='https://mychords.net'
        url='https://mychords.net/search?q='+name

        r=requests.get(url)
        b=bs4.BeautifulSoup(r.text, "html.parser")
        #list_music=b.select('.inline-v-middle .b-listing__item')
        list_music=b.select('.b-listing__item')
        print()
        message=''
        for i in range(len(list_music)):

            pri=list_music[i].find('a')

            href=list_music[i].find('a').get('href')

            pri=pri.getText().strip()

            message=message+(str(i+1) +')'+ str(pri) +'\n'+ str(name_url+href)+'\n---------------\n')
    except:
        message='Неизвестная ошибка'
    return message